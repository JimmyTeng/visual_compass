//
// Created by jimmyteng on 19-6-8.
//

#ifndef VISUALCOMPASS_BASE_MACRO_DEFINE_H
#define VISUALCOMPASS_BASE_MACRO_DEFINE_H

#define SINGLETON_MACRO(class_name)\
    private:\
    class_name() = default;\
    public:\
    static class_name *GetInstance(){\
    static class_name* instance = new (class_name) ();\
    return instance;}\
    private:

#define NO_ERROR                    ( 0)
#define ERROR_RUN_FAIL              (-1)
#define ERROR_INPUT_DATA_ERROR      (-2)
#define ERROR_OUTOF_MEMORY          (-3)
#define ERROR_TIME_OUT              (-4)
#define ERROR_PERMISSION_DENIED     (-5)
#define ERROR_UNINITIALIZE          (-6)

#endif //VISUALCOMPASS_BASE_MACRO_DEFINE_H
