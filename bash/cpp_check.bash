#!/bin/bash
cd ..
rm -rf ./cpp_check
mkdir -p cpp_check
cd cpp_check
cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=ON ..
cppcheck --project=compile_commands.json
cd ..
rm -rf ./check

