#!/bin/bash
cd ..
rm -rf ./build
mkdir -p build
cd build
cmake -DCMAKE_INSTALL_PREFIX=../../../build/ ..
make -j7
make test
test_res=$?

echo "state: " $test_res

if [ $test_res == 0 ]; then
    make install
else
    echo "Unit _DetectOrientation have not passed!"
fi
cd ..
rm -rf ./build

